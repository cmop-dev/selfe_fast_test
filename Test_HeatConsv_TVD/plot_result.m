% Plot out results for comp.
clear all; close all;
old=load('2heat.dat.0'); %time, num., analytical
new=load('2heat.dat');
subplot(2,1,1);
plot(old(:,1),old(:,2),'b',old(:,1),old(:,3),'r');
title('Heat Consv. test: TVD: Old results');
legend('Numerical','Analytical','Location','NorthEastOutside');
ylabel('Total Heat');
subplot(2,1,2);
plot(new(:,1),new(:,2),'b',new(:,1),new(:,3),'r');
title('New results');
legend('Numerical','Analytical','Location','NorthEastOutside');
xlabel('Time (days)'); ylabel('Total Heat');

print('-dpng','out.png');
%Make sure exit for batch mode
quit
