#!/usr/bin/perl -w

# Extract time series outputs
if (@ARGV != 3)
{ print "USAGE: $0 <start> <end> <testnm>\n"; exit(1); }
$start=$ARGV[0]; $end=$ARGV[1]; $test=$ARGV[2];

system "rm -f ForPlot_2heat.dat; ./analyze_heat >& scrn.out; mv -f 2heat.dat ForPlot_2heat.dat";

#Plot
#system "matlab -nosplash -display :192 -r plot_result < /dev/null > matlab.out 2>&1";
#if(-e "out.png")
#  {system "mv out.png ../Images/$test.png";}
#else
#  {
#    open(FL,">tmp2"); print FL "Image failed in $test\n"; close(FL);
#    system "cat tmp2 >> ../Images/error.log";
#  }

