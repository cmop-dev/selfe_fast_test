#!/usr/bin/env python
"""
Test numerical equivalance of two sets of NetCDF4 files using np.allclose().

jesse.e.lopez
"""
#------------------------------------------------------------------------------
# Imports
#------------------------------------------------------------------------------
import os
import sys
import glob
import argparse

import numpy as np
import netCDF4 as nc


#------------------------------------------------------------------------------
# Constants
#------------------------------------------------------------------------------
VAR_2D = ['hvel', 'dahv', 'bed_load']


#------------------------------------------------------------------------------
# Functions
#------------------------------------------------------------------------------
def idFiles(dir, searchStr='*.nc'):
    """Identify and return list of files in dir."""
    dir = os.path.join(dir, searchStr)
    files = glob.glob(dir)

    return files


def compareFileSets(dir_a, dir_b, name=None):
    """Compare sets of files for numerical equality and return result."""
    result = True
    # ID files and return false if file sets differ
    dir_a_files = idFiles(dir_a)
    dir_b_files = idFiles(dir_b)
    files_a = [f.split('/')[-1] for f in dir_a_files]
    files_b = [f.split('/')[-1] for f in dir_b_files]
    dir_diff = list(set(files_a).difference(set(files_b)))
    if dir_diff:
        print 'The files in directories {0} and {1} differ:'.format(dir_a, dir_b)
        print dir_a
        for f in dir_a_files:
            print ' - '+f
        print dir_b
        for f in dir_b_files:
            print ' - '+f
        result = False
        return False

    output = ' Comparing files in {0} and {1}'.format(dir_a, dir_b)
    if name:
        output = output+' for test {0}'.format(name)
    print output

    # Test files for being close
    for f in files_a:
        fname = f.split('.')[0]
        tmp_var = fname.split('_')
        var = '_'.join(tmp_var[1:])

        print '  - Comparing %s' % f
        file_a = os.path.join(dir_a, f)
        file_b = os.path.join(dir_b, f)
        try:
          if sum([v in var for v in VAR_2D]) == 0:
              data_a = nc.Dataset(file_a).variables[var][:]
              data_b = nc.Dataset(file_b).variables[var][:]
              r = np.allclose(data_a, data_b)
          else:
              for dir in ['_x', '_y']:
                  var_2d = var+dir
                  data_a = nc.Dataset(file_a).variables[var_2d][:]
                  data_b = nc.Dataset(file_b).variables[var_2d][:]
                  r = np.allclose(data_a, data_b)
        except:
            print 'Error in comparison'
            continue
        if r is False:
            print '-- FAIL --'
            result = False

    if result is False:
        print ' FAILED'
    elif len(files_a) == 0:
        print ' FAILED'
        print ' - (no files to compare)'
    else:
        print ' PASSED'
    print ''

    return result


def exit_wrapper(result):
    if result:
        sys.exit(0)
    else:
        sys.exit(1)

#------------------------------------------------------------------------------
# Main
#------------------------------------------------------------------------------
def parseCommandLine():
    parser = argparse.ArgumentParser()
    parser.add_argument('reference', type=str,
                        help='path to reference directory')
    parser.add_argument('new', type=str,
                        help='path to new directory')
    parser.add_argument('-n', '--name', type=str, default=None,
                        help='name of test')
    args = parser.parse_args()
    result = compareFileSets(args.reference, args.new, args.name)
    exit_wrapper(result)

if __name__ == '__main__':
    parseCommandLine()
