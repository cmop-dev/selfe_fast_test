#!/bin/bash
#
# Run SELFE test and compare against reference  
#

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 selfe"
    exit 1
fi

if [ "$(uname)" == "Darwin" ]; then
    SELFE=`greadlink -f $1`
else
    SELFE=`readlink -f $1`
fi

NP=2
echo "SELFE fast test"
echo "- Using $NP cores"

exit_code=0

#for t in {\
#Test_Btrack_Cone_Linear,\
#Test_Btrack_Cone_Quad}
for t in Test_*; 
do
    echo "- $t"
    cd $t
    rm -rf outputs/*
    rm -rf new
    echo "- $t" >> ../run.log
    echo "- $t" >> ../combine.log
    mpirun -np $NP $SELFE >> ../run.log
    autocombine.py -j 10 -o new 1 1 >> ../combine.log
    python ../scripts/CompareSelfeOutputs.py reference new 
    if [[ $? -ne 0 ]]; 
    then
        echo "FAIL"
        exit_code=1
    fi
    cd ..
    rm -rf new
    rm -rf outputs/*
done

exit $exit_code
