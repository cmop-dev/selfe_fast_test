#!/bin/sh
# If this is updated, you need to manually move it .git/pre-commit
# - make sure it is executable too

# Rename build script and update git
echo "In pre-commit hook:"
old=`ls update_*`
echo "- old $old"
new_num=`git rev-parse --short HEAD`
new="update_$new_num.sh"
echo "- new $new"
git mv $old $new

# Update Dockerfile so cache is not used 
# - Need to use .bak because of BSD sed
sed -i.bak "s/$old/$new/" Dockerfile
git add Dockerfile
rm -f Dockerfile.bak
