#!/bin/bash 
for d in Test_*; 
do
  cd $d

  rm -rf fort.*
  rm -rf combine*
  rm -rf genc*
  rm -rf *.sub 
  rm -rf *.out 
  rm -rf *.0
  rm -rf *.bak 
  rm -rf *.py 
  rm -rf *.log
  rm -rf *.yaml
  rm -rf *.dat
  rm -rf test_*
  rm -rf new/*
  rm -rf log/
  rm -rf outputs/*

  cd ..
done
