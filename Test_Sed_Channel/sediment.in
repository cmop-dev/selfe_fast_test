!
!  Non-cohesive Sediment Model Parameters.
!!======================================================================
!! August, 2007                                                        !
!!======================================================Ligia Pinto=====
!!                                                                     !
!! This file is adapted from ROMS:                                     !
!!                                                                     !
!!======================================================================
!
!========================================================= Hernan G. Arango ===
!  Copyright (c) 2002-2007 The ROMS/TOMS Group                                !
!    Licensed under a MIT/X style license                                     !
!    See License_ROMS.txt                                                     !
!==============================================================================
!                                                                             !
! Input parameters can be entered in ANY order, provided that the parameter   !
! KEYWORD (usually, upper case) is typed correctly followed by "="  or "=="   !
! symbols. Any comment lines are allowed and must begin with an exclamation   !
! mark (!) in column one.  Comments may  appear to the right of a parameter   !
! specification to improve documentation.  All comments will ignored during   !
! reading.  Blank lines are also allowed and ignored. Continuation lines in   !
! a parameter specification are allowed and must be preceded by a backslash   !
! (\).  In some instances, more than one value is required for a parameter.   !
! If fewer values are provided, the  last value  is assigned for the entire   !
! parameter array.  The multiplication symbol (*),  without blank spaces in   !
! between, is allowed for a parameter specification.  For example, in a two   !
! grids nested application:                                                   !
!                                                                             !
!    AKT_BAK == 2*1.0d-6  2*5.0d-6              ! m2/s                        !
!                                                                             !
! indicates that the first two entries of array AKT_BAK,  in fortran column-  !
! major order, will have the same value of "1.0d-6" for grid 1,  whereas the  !
! next two entries will have the same value of "5.0d-6" for grid 2.           !
!                                                                             !
! In multiple levels of nesting and/or multiple connected domains  step-ups,  !
! "Ngrids" entries are expected for some of these parameters.  In such case,  !
! the order of the entries for a parameter is extremely important.  It  must  !
! follow the same order (1:Ngrids) as in the state variable declaration. The  !
! USER may follow the above guidelines for specifying his/her values.  These  !
! parameters are marked by "==" plural symbol after the KEYWORD.              !
!                                                                             !
!==============================================================================
!
!------------------------------------------------------------------------------
!  General sediment bed model controls.
!------------------------------------------------------------------------------
! Depositional bed layer thickness criteria to create a new layer (m). If
! deposition exceeds this value, then a new layer is created, [1:Ngrids].
!
    newlayer_thick == 5.0d0
!
! Bed load transport rate coefficient. [1:Ngrids].
!
    bedload_coeff == 0.0d0
!------------------------------------------------------------------------------
! Non-cohesive Sediment Parameters, [1:NNS,1:Ngrids] values expected.
!------------------------------------------------------------------------------
!
! Median sediment grain diameter (mm).
!
   sand_sd50 == 0.05d0
!
! Sediment grain density (kg/m3).
!
   sand_srho == 2650.0d0
!
! Particle settling velocity (mm/s).
!
   sand_wsed == 1.0d0
!
! Surface erosion rate (kg/m2/s). E0 ()
!
   sand_erate == 5.0e-5
!
! Critical shear for erosion and deposition (N/m2).
!
   sand_tau_ce == 0.05d0
!
! Porosity (nondimensional: 0.0-1.0):  Vwater/(Vwater+Vsed).
!
  sand_poros == 0.9d0

! Morphological time scale factor (greater than or equal to 1.0). A
! value of 1.0 has no scale effect.
!
  sand_morph_fac == 1.0d0

!------------------------------------------------------------------------------
! Additional parameters for sediment model 
!------------------------------------------------------------------------------

! Print debug statements in sediment model : 0=no, 1=outputs
  sed_debug == 1

! Compute bedload
  bedload == 0

! Compute suspended load
  suspended_load == 1

! Compute morphological changes
  sed_morph == 1

! Slope formulation of MPM bedload formulation
  slope_formulation == 1

! Boundary condition for WENO
  bc_for_weno == 2

! Number of beds
  Nbed == 1

! Porosity 
  porosity == 0.9

! von Karman constant
  vonKar == 0.4

! Max bottom stress
  Cdb_min == 0.000001

! Min bottom stress
  Cdb_max == 0.5

! Bedthick ness
  bedthick_overall == 10.0

! Drag formulation
  drag_formulation == 1

! Mean rho
  rhom == 1000.0

!
!------------------------------------------------------------------------------
! Bed layer and bottom sediment parameters, [1:Ngrids] values expected.
!------------------------------------------------------------------------------

! Hout(ithck) == T                               ! sediment layer thickness
! Hout(iaged) == T                               ! sediment layer age
! Hout(iporo) == T                               ! sediment layer porosity
! Hout(idiff) == F                               ! biodiffusivity

!
!  GLOSSARY:
!  =========
!
!------------------------------------------------------------------------------
!  Sediment model control switch, [1:Ngrids].
!------------------------------------------------------------------------------
!
!  Lsediment       Switch to control sediment model computation within nested
!                    and/or multiple connected grids. By default this switch
!                    is set to TRUE in "mod_scalars" for all grids. The USER
!                    has the option, for example, to compute sediment in just
!                    one of the nested grids. If so, this switch needs to be
!                    consistent with the dimension parameter NST in input
!                    script (ocean.in).  In order to make the model more
!                    efficient in  memory usage, NST(:) should be zero in
!                    such grids.
!
!------------------------------------------------------------------------------
!  General sediment bed model controls, [1:Ngrids] values are expected.
!------------------------------------------------------------------------------
! 
!  NEWLAYER_THICK  Depositional bed layer thickness criteria to create a new
!                    layer (m). If deposition exceeds this value, then a new
!                    layer is created.
!
!  BEDLOAD_COEFF   Bed load transport rate coefficient.
!
!------------------------------------------------------------------------------
! Suspended Non-cohesive Sediment KEYWORDS, [1:NNS,1:Ngrids] values expected.
!------------------------------------------------------------------------------
!
!  SAND_SD50       Median sediment grain diameter (mm).
!
!  SAND_CSED       Sediment concentration (kg/m3). It may be used to initialize
!                    sediment fields using analytical expressions.
!
!  SAND_SRHO       Sediment grain density (kg/m3).
!
!  SAND_WSED       Particle settling velocity (mm/s).
!
!  SAND_Erate      Surface erosion rate (kg/m2/s).
!
!  SAND_TAU_CE     Critical shear for erosion (N/m2).
!
!  SAND_TAU_CD     Critical shear for deposition (N/m2).
!
!  SAND_POROS      Porosity (nondimensional: 0.0-1.0):  Vwater/(Vwater+Vsed).
!
!  SAND_TNU2       Lateral, harmonic, constant, mixing coefficient (m2/s),
!                    TNU2(idsed(i)) with i=NCS+1:NST.  If variable horizontal
!                    diffusion is activated, TNU2 is the mixing coefficient
!                    for the largest grid-cell in the domain.
!
!  SAND_TNU4       Lateral, biharmonic, constant, mixing coefficient (m4/s),
!                    TNU4(idsed(i)) with i=NCS+1:NST.  If variable horizontal
!                    diffusion is activated, TNU4 is the mixing coefficient
!                    for the largest grid-cell in the domain.
!
!  SAND_AKT_BAK    Background vertical mixing coefficient (m2/s),
!                    AKT_BAK(idsed(i)) with i=NCS+1:NST.
!
!  SAND_TNUDG      Nudging time scale (days), TNUDG(idsed(i)) with i=NCS+1:NST.
!                    Inverse scale will be computed internally,
!
!  SAND_MORPH_FAC  Morphological time scale factor (nondimensional; greater
!                    than or equal to 1.0). A value of 1.0 has no scale effect.
!
!  Hout(idsand)    Logical switches to activate writing of non-cohesive
!                     sediment concentration into HISTORY NetCDF file,
!                     HOUT(idTvar(idsed(i))) with i=1:NCS+1,NST.
!
!  Hout(iSfrac)    Logical switches to activate writing of non-cohesive
!                     sediment class fraction composition of each bed layer
!                     into HISTORY NetCDF file, HOUT(idfrac(i)) with
!                     i=NCS+1,NST.
!
!  Hout(iSmass)    Logical switches to activate writing of non-cohesive
!                    sediment mass of each bed layer into HISTORY NetCDF file,
!                    HOUT(idsed(i)) with i=NCS+1,NST.
!
!  Hout(iSUbld)    Logical switches to activate writing of non-cohesive
!                    sediment bed load at U-points into HISTORY NetCDF file,
!                    HOUT(idsed(i)) with i=NCS+1,NST.
!
!  Hout(iSVbld)    Logical switches to activate writing of non-cohesive
!                     sediment bed load at V-points into HISTORY NetCDF file,
!                     HOUT(idsed(i)) with i=NCS+1,NST.
!
!------------------------------------------------------------------------------
! Bed layer and bottom sediment KEYWORDS, [1:Ngrids] values expected.
!------------------------------------------------------------------------------
!
!  Hout(ithck)     Sediment layer thickness.
!
!  Hout(iaged)     Sediment layer age.
!
!  Hout(iporo)     Sediment layer porosity.
!
!  Hout(idiff)     Biodiffusivity at the bottom of each layer.
