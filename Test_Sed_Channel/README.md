Sediment : CSTM-ROMS open channel test
======================================

Summary:
--------
This is the open channel test from the Warner et al. 2008 paper on the CSTM model.

To run:
-------
- You can run the whole thing with warner_test.sh
- testWarner.py makes plots
