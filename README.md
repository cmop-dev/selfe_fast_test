# Megafast SELFE test 

Very fast test used to check for absolute blunders in commits.

Changes to repo launches automatic build of Docker container.
- Uses a git hook to update the Dockerfile
- Uses ENV and file edits to Dockerfile to force git pull to avoid caching
- The line to run the update_nnnnnn.sh script in Dockerfile is updated with the git hash
- If update_nnnnnns.sh doesn't match the file name in directory it will not update
- See scripts/pre-commit_hook.sh
