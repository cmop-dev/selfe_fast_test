Backtrack Cone Test
===================

Description
-----------
- A cone of elevated temperature rotated around the circular domain.
- The gen_ic.f90 files generate the temperature initial conditions for the simulation.
- Compare reference and new simulation using np.all_close() for every file.

Horizontal mesh
---------------
- Elements: 2304 
- Nodes: 1201

Vertical mesh
-------------
- Levels: 3 

Output files
------------
- elev.61
- temp.63
- salt.63
- hvel.64

Simulation time
---------------
- 3000s 

Time step
---------
- 50s

Evaluation
----------
- 
