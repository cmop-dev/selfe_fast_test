#!/usr/bin/perl -w

# Extract time series outputs
if (@ARGV != 3)
{ print "USAGE: $0 <start> <end> <testnm>\n"; exit(1); }
$start=$ARGV[0]; $end=$ARGV[1]; $test=$ARGV[2];

#Prepare screen inputs for extraction
open(RE,">read.in");
print RE "elev.61\n $end\n 60686.083149 16316.292042\n 0\n";
close(RE);
system "rm -f ForPlot_elev.dat; cd outputs/; ../read_output7b < ../read.in >& scrn.out; 
        mv -f extract.out ../ForPlot_elev.dat";

open(RE,">read.in");
print RE "dahv.62\n $end\n 109591.546948, 52857.550268\n 0\n";
close(RE);
system "rm -f ForPlot_vel.dat; cd outputs/; ../read_output7b < ../read.in >& scrn.out; 
        mv -f extract.out ../ForPlot_vel.dat";

