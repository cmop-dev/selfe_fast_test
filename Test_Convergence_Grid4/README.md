Backtrack Gausshill Test
========================

Description
-----------
- Convergence test for quarter annulus with non-symmetric wth a grid
- Grid A from Elfe_Tests1/quarter-Annulus
- Compare reference and new simulation using np.all_close() for every file.
- 2D test, GOTM cannot be compiled

Reference
---------
- OHSU SVN r502, 1 cpu

Horizontal mesh
---------------
- Element: 24 
- Nodes: 20

Vertical mesh
-------------
- N/A 

Output files
------------
- dahv.62 
- elev.61

Simulation time
---------------
- 432000 

Time step
---------
- 600 

Time to solution
----------------
- ~1 sec 1 Sandy Bridge Xeon 
